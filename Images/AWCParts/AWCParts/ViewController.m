//
//  ViewController.m
//  AWCParts
//
//  Created by Satish Kumar Baswapuram on 4/28/14.
//  Copyright (c) 2014 Satish Kumar Baswapuram. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property NSMutableArray * timeGroups;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    
    NSURL * passwordURL = [NSURL URLWithString:@"https://docs.google.com/document/d/1hoKxvD-7gNkUGVj0nzOeJbCEDDoYvRc59GSrmj84SPg/export?format=txt&id=1hoKxvD-7gNkUGVj0nzOeJbCEDDoYvRc59GSrmj84SPg"];  //@"https://dl.dropboxusercontent.com/s/y7gxs251cuvaw3h/password.json?dl=1&token_hash=AAHhKQBFtwG2OsSId3ROpEQsc_zZGCeTGbHQRwX4bVCi0g"];
    NSData * passwordData = [NSData dataWithContentsOfURL:passwordURL];
    NSError * error = nil;
    NSDictionary * passwordDictionary = [NSJSONSerialization JSONObjectWithData:passwordData options:0 error:&error];
    
    NSString * password = passwordDictionary[@"password"];
    
    UILabel * passwordLabel = [[UILabel alloc]initWithFrame:CGRectMake(200, 200, 150, 50)];
    passwordLabel.text = password;
    
    [self.view addSubview:passwordLabel];
    NSLog(@"Password: %@",password);
    
    [self.presentButtons setTarget:self];
    [self.presentButtons setAction:@selector(showButtons:)];
    
    self.timeGroupsVC = [[TimeGroupsVC alloc]initWithStyle:UITableViewStylePlain];
    self.timeGroupsVC.delegate = self;
    
    self.popOverController = [[UIPopoverController alloc]initWithContentViewController:self.timeGroupsVC];
    
    self.timeGroups = [[NSMutableArray alloc]init];
    
    
//    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
//    [dateFormatter setDateFormat:@"z"];
//    NSString * timeZone = [dateFormatter stringFromDate:[NSDate date]];
    double timeZoneHours = ([[NSTimeZone localTimeZone] secondsFromGMT])/3600.0;
    
    //NSLog(@"You are in %@",timeZone);
    
    NSString * receivedTime = @"12:52:00";
    
    //NSLog(@"Server time: %@",receivedTime);
    
    //int hrs = [[receivedTime substringWithRange:NSMakeRange(0, 2)] intValue];
    //int seconds = [[receivedTime substringWithRange:NSMakeRange(3, 2)] intValue];
    
    double localTime = [[[receivedTime substringWithRange:NSMakeRange(0, 5)] stringByReplacingOccurrencesOfString:@":" withString:@"."] doubleValue];
    NSLog(@"Server Time: %.2f",localTime);
    
    //timeZoneHours = -5.5;
    
    int quotient = timeZoneHours/0.5;
    double extraSeconds = 0;

    if(abs(quotient%2)==1)
    {
        if(timeZoneHours>=0)
            timeZoneHours -= 0.2;
        else
        {
            timeZoneHours += 0.2;
            extraSeconds = 0.40;
        }
    }

    //NSLog(@"Sum: %f",hrs+seconds/100.0);
    
    localTime += timeZoneHours-extraSeconds;
    if(localTime<0)
        localTime = 24 + localTime;
    else if(localTime>=24)
        localTime = localTime - 24;

    NSLog(@"Local time: %2.2f TimeZoneHrs: %2.2f",localTime,timeZoneHours);

    if((localTime-(int)localTime)*100>=60)
    {
        localTime -= 0.60;
        localTime++;
    }
    
    NSLog(@"Local time: %2.2f TimeZoneHrs: %2.2f",localTime,timeZoneHours);
    
//    hrs -= timeZoneHours;
//    if(hrs>24)
//        hrs -= 24;

    
    //NSLog(@"Your local time is %d:%d",hrs,seconds);
    
    
    self.timeZoneLBL.text = [NSString stringWithFormat:@"%.2f",localTime];
    
    
}

-(void)selectedTimeGroup:(NSString *)timeGroup
{
    //NSLog(@"Received Time Group: %@",timeGroup);
    //[self.popOverController dismissPopoverAnimated:YES];
    [self.timeGroups addObject:timeGroup];
    NSLog(@"Added: %@",self.timeGroups);
}

-(void)deselectedTimeGroup:(NSString *)timeGroup
{
    //NSLog(@"Removed Time Group: %@",timeGroup);
    [self.timeGroups removeObject:timeGroup];
    NSLog(@"Removed: %@",self.timeGroups);
}

-(void)showButtons:(id)sender
{
    [self.popOverController presentPopoverFromBarButtonItem:self.presentButtons permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
