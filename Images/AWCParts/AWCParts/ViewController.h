//
//  ViewController.h
//  AWCParts
//
//  Created by Satish Kumar Baswapuram on 4/28/14.
//  Copyright (c) 2014 Satish Kumar Baswapuram. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TimeGroupsVC.h"

@interface ViewController : UIViewController <TimeGroupDelegate>

@property (weak, nonatomic) IBOutlet UILabel *timeZoneLBL;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *presentButtons;

@property TimeGroupsVC * timeGroupsVC;
@property UIPopoverController * popOverController;

@end
