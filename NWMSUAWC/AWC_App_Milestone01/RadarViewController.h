//
//  RadarViewController.h
//  GA Weather
//
//  Created by Venkatesh Samudrala on 9/7/14.
//  Copyright (c) 2014 Satish Kumar Baswapuram. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>


@interface RadarViewController : UIViewController<MKMapViewDelegate>
@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentType;
- (IBAction)segementAction:(UISegmentedControl *)sender;

@end
