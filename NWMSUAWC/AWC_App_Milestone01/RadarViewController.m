//
//  RadarViewController.m
//  GA Weather
//
//  Created by Venkatesh Samudrala on 9/7/14.
//  Copyright (c) 2014 Satish Kumar Baswapuram. All rights reserved.
//

#import "RadarViewController.h"

@implementation RadarViewController

- (IBAction)segementAction:(UISegmentedControl *)sender {
    
    
    if (self.segmentType.selectedSegmentIndex==0) {
        NSLog(@"1st");
        self.mapView.mapType=MKMapTypeSatellite;
    }else{
        NSLog(@"2nd");
        self.mapView.mapType=MKMapTypeStandard;
    }
    
}


-(void)viewWillAppear:(BOOL)animated{
    self.mapView.mapType=MKMapTypeSatellite;
}
@end
